//
//  UIViewExtension.swift
//  iOStest
//
//  Created by Imthath M on 09/07/19.
//  Copyright © 2019 Zoho Corp. All rights reserved.
//

import UIKit

extension UIView {
    
    @discardableResult func align(_ type1: NSLayoutConstraint.Attribute,
                                  with view: UIView? = nil, on type2: NSLayoutConstraint.Attribute? = nil,
                                  offset constant: CGFloat = 0,
                                  priority: Float? = nil) -> NSLayoutConstraint? {
        guard let view = view ?? superview else {
            return nil
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        let type2 = type2 ?? type1
        let constraint = NSLayoutConstraint(item: self, attribute: type1,
                                            relatedBy: .equal,
                                            toItem: view, attribute: type2,
                                            multiplier: 1, constant: constant)
        if let priority = priority {
            constraint.priority = UILayoutPriority.init(priority)
        }
        
        constraint.isActive = true
        
        return constraint
    }
    
    func alignEdges(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.top, with: view, offset: constant)
        align(.bottom, with: view, offset: -constant)
        align(.leading, with: view, offset: constant)
        align(.trailing, with: view, offset: -constant)
    }
    
    func align(greaterThanHeight height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .greaterThanOrEqual,
                                         toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height))
    }
    
    func alignHorizontally(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.leading, with: view, offset: constant)
        align(.trailing, with: view, offset: -constant)
    }
    
    func alignVertically(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.top, with: view, offset: constant)
        align(.bottom, with: view, offset: -constant)
    }
    
    func alignTop(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.top, with: view, offset: constant)
        align(.leading, with: view, offset: constant)
        align(.trailing, with: view, offset: -constant)
    }
    
    func alignBottom(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.bottom, with: view, offset: -constant)
        align(.leading, with: view, offset: constant)
        align(.trailing, with: view, offset: -constant)
    }
    
    func alignCenter(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.centerX, with: view)
        align(.centerY, with: view)
    }
    
    func alignLeading(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.top, with: view, offset: constant)
        align(.bottom, with: view, offset: -constant)
        align(.leading, with: view, offset: constant)
    }
    
    func alignTrailing(with view: UIView? = nil, offset constant: CGFloat = 0) {
        align(.top, with: view, offset: constant)
        align(.bottom, with: view, offset: -constant)
        align(.trailing, with: view, offset: -constant)
    }
    
    func align(_ attributes: [NSLayoutConstraint.Attribute], with view: UIView? = nil, offset constant: CGFloat = 0) {
        for attribute in attributes {
            align(attribute, with: view, offset: constant)
        }
    }
}

extension UIView {
    
    enum SizeAttribute {
        case height
        case width
    }
    
    func fixSize(_ size: CGSize) {
        fixWidth(size.width)
        fixHeight(size.height)
    }
    
    func fixWidth(_ width: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func fixHeight(_ height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func make(_ sizeAttribute: SizeAttribute, fraction multiplier: CGFloat = 1, as view: UIView, offset: CGFloat = 0) {
        self.getAnchor(for: sizeAttribute).constraint(equalTo: view.getAnchor(for: sizeAttribute),
                                                      multiplier: multiplier, constant: offset).isActive = true
    }
    
    private func getAnchor(for sizeAttribute: SizeAttribute) -> NSLayoutDimension {
        switch sizeAttribute {
        case .height:
            return self.heightAnchor
        case .width:
            return self.widthAnchor
        }
    }
}

extension UIView {

//    enum SideAttribute {
//        case leading
//        case trailing
//        case top
//        case bottom
//    }
    
    func alignSafe(with view: UIView) {
        let guide = view.getLayoutGuide()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
    }
    
    func alignTopSafe(with view: UIView) {
        let guide = view.getLayoutGuide()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        self.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
    }
    
    func alignBottomSafe(with view: UIView) {
        let guide = view.getLayoutGuide()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
        self.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
    }
    
    func alignLeadingSafe(with view: UIView) {
        let guide = view.getLayoutGuide()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
    }
    
    func alignTrailingSafe(with view: UIView) {
        let guide = view.getLayoutGuide()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
    }
    
    
    func getLayoutGuide() -> UILayoutGuide {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide
        }
        
        return self.layoutMarginsGuide
    }
}
